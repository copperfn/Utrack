module Main exposing (main)

import Browser
import Html.Styled.Attributes exposing (css)
import Css exposing (minHeight, pct, minWidth, top, left, position, absolute, displayFlex, px, backgroundColor)
import Html.Styled exposing (div, toUnstyled)
import List exposing (singleton)
import Model exposing (Model, Msg(..), State(..), Branch(..), Page, update, init)

import View.UtrackComponent exposing (palette)
import View.Home as Home
import View.Load.Builder as Load

main : Program () Model Msg
main =
    Browser.application
        {init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = RequestUrl
        , onUrlChange = ChangeUrl
        }

view : Model -> Browser.Document Msg
view model = {title = "Utrack"
             , body =
                 pageFromState model
             |> div [css [minHeight (pct 100)
                         ,minWidth (pct 100)
                         ,top (px 0)
                         ,left (px 0)
                         ,position absolute
                         ,displayFlex
                         ,backgroundColor <| palette.light.backGround
                         ]
                    ]
             |> toUnstyled
             |> singleton
             }

pageFromState : Model -> Page
pageFromState model =
    case model.state of
        Home -> Home.home
        Loading branch -> pageFromLoad model branch
        Branch branch -> pageFromBranch model branch

pageFromBranch : Model -> Branch -> Page
pageFromBranch model branch =
    case branch of
        Play -> []
        Builder -> []

pageFromLoad : Model -> Branch -> Page
pageFromLoad model branch =
    case branch of
        Play -> []
        Builder -> Load.builder model

subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
