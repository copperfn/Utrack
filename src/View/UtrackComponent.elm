module View.UtrackComponent exposing (flexBox, button, palette, flexDefault, card, input, fileInput)

import Html.Styled as Ht exposing (text)
import Css exposing (..)
import Html.Styled.Attributes exposing (..)
import Model exposing (Msg(..), Model, Inputs(..), InputId, inputToId)
import Basics.Extra exposing (flip)
import Html.Styled.Events exposing (..)
import Dict exposing (..)

type alias Attribute = Ht.Attribute Msg
type alias Html = Ht.Html Msg


type alias Theme = {primary: Color
                   ,secondary: Color
                   ,tertiary: Color
                   ,accent: Color
                   ,backGround: Color
                   }

light : Theme
light = {primary = hex "#00BD9D"
        ,secondary = hex "#172A3A"
        ,tertiary = hex "#F5F5F7"
        ,accent = hex "#fe6d73"
        ,backGround = hex "#cabdb9"
        }

palette : {light: Theme}
palette = {light = light}

mergeAttribute : List Style -> List Attribute -> Style -> List Attribute
mergeAttribute customStyle attributs defaultStyle = css (defaultStyle :: customStyle) :: attributs

flexDefault : Style
flexDefault =
    batch [displayFlex
          ,flex (int 1)
          ,alignItems center
          ,justifyContent center
          ,overflow Css.hidden
          ]


flexBox : List Style -> List Attribute -> List Html -> Html
flexBox customStyles attributes body =
    flip Ht.div body
        <| mergeAttribute customStyles attributes flexDefault


button : List Style -> List Attribute -> List Html -> Html
button customStyles attributes body =
    flip Ht.button body
        <| mergeAttribute customStyles attributes buttonStyle

buttonStyle : Style
buttonStyle = batch [backgroundColor light.primary
                    ,color light.secondary
                    ,border3 (px 2) solid light.secondary
                    ,borderRadius <| em 3
                    ,padding <| em 0.5
                    ,margin <| em 0.5
                    ,fontSize <| pt 13
                    ,hover [backgroundColor light.secondary
                           ,color light.accent
                           ]
                    ]

separator : Html
separator =
    Ht.div [css [Css.height <| px 0
                ,minWidth <| pct 100
                ,borderBottom3 (px 1) solid <| light.secondary
                ]] []

card : List Style -> List Attribute -> List Html -> Html
card customStyle attribute body =
    flexBox (cardStyle :: customStyle) attribute <| List.intersperse separator body

cardStyle : Style
cardStyle =
    batch [boxShadow5 (px 0) (px 0) (px 1) (px 1) <| light.secondary
          ,backgroundColor <| light.tertiary
          ]

input: List Style -> List Attribute -> List Html -> Html
input customStyle attributes body =
    Ht.label [css customStyle]
        [Ht.input attributes body]

fileInput: List Style -> Model -> Html
fileInput customStyle model =
    Ht.div [css [flexDefault
                ,flexDirection row
                ]
           ]
        <| (Ht.label [css <| buttonStyle
                  :: displayFlex
                  :: customStyle
                  ]
             [ text "Load Builder"
             ,Ht.input [(css [display none])
                       ,type_ "file"
                       ,onInput <| Input FileInput
                       ] []
             ]
        :: (maybeRender << Maybe.map renderFileName <| get (inputToId FileInput) model.inputs))
        ++ (Maybe.withDefault [] <| Maybe.map (List.map renderError) <| get (inputToId FileInput) model.errors)


maybeRender : Maybe Html -> List Html
maybeRender mHtml =
    case mHtml of
        Just html -> [html]
        Nothing -> []

renderFileName : String -> Html
renderFileName fakeFileName =
    let fileName = String.dropLeft 12 fakeFileName in
    flexBox [backgroundColor <| light.backGround
            ,border3 (px 1) solid light.secondary
            ,position relative
            ] []
    [text fileName]


renderError : String -> Html
renderError error = Ht.div [css [backgroundColor <| hex "#ffc0cb"
                                ,border3 (px 1) solid <| hex "#f00"
                                ,padding2 (em 0.25) (em 0.5)
                                ]
                           ]
                    [text error]
