module View.Home exposing (home)

import Html.Styled exposing (Html, div, text, h1, a)
import Html.Styled.Attributes exposing (..)
import Model exposing (Msg)
import Css exposing (..)
import View.UtrackComponent exposing (..)

type Side = Right
          | Left

imageSrc : Side -> String
imageSrc side =
    case side of
        Left -> "/src/asset/playCover.jpeg"
        Right -> "/src/asset/buildCover.png"

buttonLabel : Side -> String
buttonLabel side =
    case side of
        Left -> "Play"
        Right -> "Build a tracker"

hoverStyle : Side -> List Style
hoverStyle side =
    case side of
        Left -> [boxShadow6 inset (px -2) (em 0) (em 0.5) (em 0.5) <| palette.light.backGround
                ,borderBottomRightRadius <| em 1
                ,borderTopRightRadius <| em 1
                ]
        Right -> [boxShadow6 inset (px 2) (px 0) (em 0.5) (em 0.5) <| palette.light.backGround
                 ,borderBottomLeftRadius <| em 1
                 ,borderTopLeftRadius <| em 1
                 ]


nextPagePath : Side -> String
nextPagePath side =
    case side of
        Left -> "/play/load"
        Right -> "/build/load"

half : Side -> Html Msg
half side =
    flexBox [backgroundColor <| palette.light.backGround] [alt <| buttonLabel side]
        [a [css [flexDefault
                ,hover <| hoverStyle side
                ,Css.width <| pct 100
                ,Css.height <| pct 100
                ,backgroundImage <| url <| imageSrc side
                ,backgroundRepeat noRepeat
                ,backgroundSize cover
                ,backgroundPosition center
                ,backgroundColor transparent
                ]
           ,href <| nextPagePath side
           ]
             [h1 [css [backgroundColor palette.light.primary
                      ,padding <| em 1
                      ,color <| hex "#fff"
                      ]]
                  [text <| buttonLabel side]]
        ]

home : List (Html Msg)
home =
    [flexBox [Css.property "align-items" "normal"] []
         [ half Left
         , half Right
         ]
    ]
