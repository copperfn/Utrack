module View.Load.Builder exposing (builder)

import Model exposing (Model, Msg(..), Page)
import View.UtrackComponent exposing (..)
import Html.Styled as Ht exposing (Html, text, div, h1)
import Html.Styled.Attributes exposing (..)
import Css exposing (..)
import List exposing (singleton)
import Html.Styled.Events exposing (onClick)

header : Html Msg
header = div [css [minHeight <| em 3
                  ,backgroundColor <| palette.light.primary
                  ,minWidth <| pct 100
                  ]
             ] []

loadATracker : Model -> Html Msg
loadATracker model =
    flexBox [flexDirection column] []
        [h1 [] [text "Edit"]
        ,fileInput [] model
        , button [] [onClick LoadTracker] [text "Load"]
        ]

newBuilder : Html Msg
newBuilder =
    button [] [onClick Build] [text "New"]

builder : Model -> Page
builder model =
    singleton <| flexBox [] []
        [card [maxWidth <| pct 30
              ,flexDirection column
              ] [] [header
                   ,loadATracker model
                   ,newBuilder
                   ]]
