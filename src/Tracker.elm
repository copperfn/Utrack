module Tracker exposing (Tracker, defaultTracker, modifyTracker, loadTracker)



type TrackerMessage = Name String

type alias Tracker = {
        name: String
    }

defaultTracker : Tracker
defaultTracker = {name = "New Tracker"
                 }


modifyTracker : TrackerMessage -> Tracker -> Tracker
modifyTracker message tracker =
    case message of
        Name newName -> {tracker | name = newName}

loadTracker : String -> Result (List String) Tracker
loadTracker _ = Err ["not Implemented yet"]
