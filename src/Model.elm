module Model exposing (Model, Msg(..), State(..), Branch(..), Inputs(..), Page, InputId, update, init, inputToId)

import Browser exposing (UrlRequest(..))
import Browser.Navigation as Nav
import Maybe exposing (Maybe(..))
import Url exposing (Url, Protocol(..))
import Maybe exposing (withDefault)
import Css exposing (url)
import Html.Styled exposing (Html)
import Task.Extra exposing (message)
import Dict exposing (..)
import Tracker exposing (Tracker, modifyTracker, loadTracker, defaultTracker)
import Tracker exposing (defaultTracker)

type alias Page = List (Html Msg)
type alias InputId = String

type alias Model =
    {url: Url
    ,state: State
    ,navKey: Nav.Key
    ,errors: Dict InputId (List String)
    ,inputs: Dict InputId String
    ,tracker: Tracker
    }



type Msg = ChangeUrl Url
         | RequestUrl UrlRequest
         | Input Inputs String
         | Error Inputs (List String)
         | Build
         | LoadTracker

type State = Home
           | Loading Branch
           | Branch Branch

type Branch = Builder
            | Play

type Inputs = FileInput

inputToId : Inputs -> InputId
inputToId input =
    case input of
        FileInput -> "FileInput"

update : Msg -> Model -> (Model, Cmd Msg)
update message model =
    Debug.log "new state: " <| case message of
        ChangeUrl _        -> (model, Cmd.none)
        RequestUrl request -> requestUrl model request
        Input key input    -> handleInput model key input
        Error key errors   -> handleErrors model key errors
        Build              -> (model, Nav.pushUrl model.navKey "/build")
        LoadTracker        -> handleTrackerLoad model

handleTrackerLoad : Model -> (Model, Cmd Msg)
handleTrackerLoad model =
    case Dict.get (inputToId FileInput) model.inputs of
        Nothing       -> (model, message <| Error FileInput ["Please input a file"])
        Just filePath ->
            case loadTracker filePath of
                Ok newTracker -> ({model | tracker=newTracker}, Cmd.none)
                Err errors -> (model, message <| Error FileInput errors)

handleInput : Model -> Inputs -> String -> (Model, Cmd Msg)
handleInput model key value =
    case key of
        FileInput -> ({model
                          | inputs = updateDict key value model.inputs
                      }
                     , Cmd.none)

handleErrors : Model -> Inputs -> List String -> (Model, Cmd Msg)
handleErrors model key errors =
    ({model
         | errors = updateDict key errors model.errors
     }
    , Cmd.none)

updateValue : a -> Maybe a -> Maybe a
updateValue newValue oldValue =
    case oldValue of
        Nothing -> Just newValue
        Just _ -> Just newValue

updateDict : Inputs -> a -> Dict InputId a -> Dict InputId a
updateDict key value dict = Dict.update (inputToId key) (updateValue value) dict


requestUrl : Model -> UrlRequest -> (Model, Cmd Msg)
requestUrl model maybeUrl =
    case maybeUrl of
        Internal url -> ({model | url = url, state = stateFromUrl url}
                        , Nav.pushUrl model.navKey <| Url.toString url
                        )
        External url -> (model, Nav.load url)

init : flags -> Url -> Nav.Key -> (Model, Cmd Msg)
init _ requestUrl_ key =
    let (state, url) = startUrl requestUrl_ in
    ({url = url
     ,state = state
     ,navKey = key
     ,errors = Dict.empty
     ,inputs = Dict.empty
     ,tracker = defaultTracker
     }
    , Nav.pushUrl key url.path
    )

startUrl : Url -> (State, Url)
startUrl url =
    case stateFromUrl url of
        Home ->  (Home, homeUrl url.host)
        Loading Builder ->  (Loading Builder, url)
        Loading Play -> (Loading Play, url)
        Branch Builder -> (Branch Builder, url)
        Branch Play -> Debug.todo "Branch Play"

stateFromUrl : Url -> State
stateFromUrl url =
    String.split "/" url.path
        |> fromUrl
        |> withDefault Home

fromUrl : List String -> Maybe State
fromUrl pathList =
    case pathList of
        ["", ""] -> Just Home
        ["", "build", "load"] -> Just <| Loading Builder
        ["", "play", "load"] -> Just <| Loading Play
        ["", "build"] -> Just <| Branch Builder
        _ -> Nothing

homeUrl : String -> Url
homeUrl host =
    { protocol = Http
    , host = host
    , port_ = Nothing
    , path = "/"
    , query = Nothing
    , fragment = Nothing
    }
